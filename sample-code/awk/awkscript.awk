#!/usr/bin/awk -f

# We want to add up the marks of students where the fields are separated by |
# Format Name | Marks 1 | Marks2 |
#
BEGIN { total= 0; FS="|"; OFS=" | " }
      {
	  sum = $2 + $3;
	  print $0, sum;
	  total += sum;
      }
END   { print "Average", (total/NR)}
